<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package dhuro
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
		<h2 class="comments-title">
			<?php
			$dhuro_comment_count = get_comments_number();
			if ( '' === $dhuro_comment_count ) {
				printf(
					/* translators: 1: title. */
					esc_html__( 'One thought on &ldquo;%1$s&rdquo;', 'dhuro' ),
					'<span>' . wp_kses_post( get_the_title() ) . '</span>'
				);
			} else {
				printf( 
					/* translators: 1: comment count number, 2: title. */
					esc_html( _nx( '%1$s thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $dhuro_comment_count, 'comments title', 'dhuro' ) ),
					number_format_i18n( $dhuro_comment_count ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
					'<span>' . wp_kses_post( get_the_title() ) . '</span>'
				);
			}
			?>
		</h2><!-- .comments-title -->

		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php
			wp_list_comments(
				array(
					'style'      => 'ol',
					'short_ping' => true,
				)
			);
			?>
		</ol><!-- .comment-list -->

		<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'dhuro' ); ?></p>
			<?php
		endif;

	endif; // Check for have_comments().

	comment_form(array(
		'class_form' => 'section-inner thin max-percentage',
		'title_reply_before' => '<h2 id="reply-title" class="comment-reply-title">',
		'title_reply' => 'Komento:',
		'title_reply_after' => '</h2>',
		'comment_field' => '<p class="comment-form-comment">' .
        '<label for="comment">' . __( 'Shkruaj komentin ketu:' ) . '</label> <br>' .
        '<textarea id="comment" name="comment" placeholder=" Komenti juaj..."></textarea>' .
        '</p>',
    'comment_notes_after' => '',
	'fields' => apply_filters( 'comment_form_default_fields', $fields ),'label_submit'=>__('Shto Komentin')
   
		));
	?>
</div><!-- #comments -->
