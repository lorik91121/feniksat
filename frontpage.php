 <?php   
   /* 

	Template Name: FrontPage

	*/
    
    get_header(); 
    ?>
      <!-- header -->
      <header>
         <!-- header inner -->
         <div  class="head_top">
            
            <!-- end header inner -->
            <!-- end header -->
            <!-- banner -->
            <section class="banner_main">
               <div class="container-fluid rmPadding">
                  <div class="row d_flex">
                     <div class="col-md-5">
                        <div class="text-bg">
                           <h1>FONDACIONI </br>DHURO</h1>
                           <strong>DHURO CKA NUK TE DUHET, MERR CKA TE DUHET!</strong>
                           <span>Dhuro Page 2022</span>
                           <a href="#myModal2" data-toggle="modal">Dhuro tani</a>
                        </div>
                     </div>
                     <div class="col-md-7 padding_right1">
                        <div class="text-img gradAnimation">
                          <img src="<?php echo get_template_directory_uri() . '/assets/images/headerPic2.png';?>" alt="#"/>
                           <h3>🖤</h3>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </header>
      <!-- end banner -->
      <!-- about -->
      <div id="about" class="about">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage">
                     <h2>Rreth Dhuro</h2>
                     <span>Ideja e kesaj webfaqe eshte qe personat te cilet kane nevoje te ‘pastrojne’ garderoben e tyre, nese nuk duan te shesin ato, por te dhurojne, permes kesaj webfaqe eshte e mundur ta bejne ate, duke nenkuptuar se nje perdorues tjeter i webfaqes tone do te mund te marre ate se cka dhurohet.Ne kemi bere te mundur qe cdokush te mund te regjistrohet ne webfaqen tone, pa pagese!</span>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-8 offset-md-2 ">
                  <div class="about_box">
                     <a class="text-bg2" href="<?php echo home_url() . '/rethe-nesh/'; ?>">Më shumë</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- about -->
	  <div class="container-fluid orbit">
		<img class ="body2" id="sun" src="<?php echo get_template_directory_uri() . '/assets/images/dhuro-logo (2).png'?>" ></img>
        
        <div class ="body2" id="gmail-orbit">
            <img class ="body2" id="gmail" src="<?php echo get_template_directory_uri() . '/assets/images/gmail.png'?>"/>
        </div>
        
        <div class ="body2" id="discord-orbit">
            <img class ="body2" id="discord" src="<?php echo get_template_directory_uri() . '/assets/images/Discord Logo Circle.png'?>"/>
        </div>
        
        <div class ="body2" id="twiter-orbit">        
            <img class ="body2" id="twiter" src="<?php echo get_template_directory_uri() . '/assets/images/twiter.png'?>"/>
        </div>
	
        
         <div class ="body2" id="github-orbit">
            <img class ="body2" id="github" src="<?php echo get_template_directory_uri() . '/assets/images/github.png'?>"/>
        </div>
	  </div>
      <!-- best -->
      <div id="" class="best">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage">
                     <h2>Dhuruesit më aktiv</h2>
                     <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</span>
                  </div>
               </div>
            </div>
            <div class="row">
				<div class="col-sm-4 hoverE">
					<div class="leaderboard-card">
						<div class="leaderboard-card__top">
						<?php
								global $wpdb;
								$sql = $wpdb->prepare("Select SUBQUERY.pt,SUBQUERY.ct from (select post_author as pt, count(*) as ct
															from wp_posts
															WHERE post_type='postimet'
															group by post_author) AS SUBQUERY
															ORDER BY SUBQUERY.ct DESC LIMIT 1 OFFSET 1");
								$res = $wpdb->get_results( $sql );
								foreach($res as $wp_ranking){}
						?>
							<h3 class="text-center"><strong><?php echo $wp_ranking->ct ;?></strong></h3>
						</div>
					<a href="<?=get_author_posts_url($wp_ranking->pt)?>" class="rmlinkStyle">
						<div class="leaderboard-card__body">
							<div class="text-center">
								<img src="<?php echo get_avatar_url($id_or_email=$wp_ranking->pt, 30);?>" class="circle-img mb-2" alt="User Img">
								<h5 class="mb-0"><strong><?php echo get_the_author_meta( 'first_name', $wp_ranking->pt ); ?>&nbsp;<?php echo get_the_author_meta( 'last_name', $wp_ranking->pt ); ?></strong></h5>
								<p class="text-muted mb-0"><i class="bi bi-envelope-fill"></i>&nbsp;<?php echo get_the_author_meta( 'email', $wp_ranking->pt ); ?></p>
								<hr>
								<div class="d-flex justify-content-between align-items-center pdtop">
									<span><i class="bi-gift-fill"></i> Dhuro</span>
								</div>
							</div>
						</div></a>
					</div>
				</div>
						<?php
								global $wpdb;
								$sql1 = $wpdb->prepare("Select SUBQUERY.pt,SUBQUERY.ct from (select post_author as pt, count(*) as ct
															from wp_posts
															WHERE post_type='postimet'
															group by post_author) AS SUBQUERY
															ORDER BY SUBQUERY.ct DESC LIMIT 1 OFFSET 0");
								$res1 = $wpdb->get_results( $sql1 );
								foreach($res1 as $wp_ranking1){}
						?>
				<div class="col-sm-4 hoverE">
					<div class="leaderboard-card leaderboard-card--first">
						<div class="leaderboard-card__top">
							<h3 class="text-center"><strong><?php echo $wp_ranking1->ct ;?></strong></h3>
						</div>
					<a href="<?=get_author_posts_url($wp_ranking1->pt)?>" class="rmlinkStyle">
						<div class="leaderboard-card__body">
							<div class="text-center">
								<img src="<?php echo get_avatar_url($id_or_email=$wp_ranking1->pt, 30);?>" class="circle-img mb-2" alt="User Img">
								<h5 class="mb-0"><strong><?php echo get_the_author_meta( 'first_name', $wp_ranking1->pt ); ?>&nbsp;<?php echo get_the_author_meta( 'last_name', $wp_ranking1->pt ); ?></strong></h5>
								<p class="text-muted mb-0"><i class="bi bi-envelope-fill"></i>&nbsp;<?php echo get_the_author_meta( 'email', $wp_ranking1->pt ); ?></p>
								<hr>
								<div class="d-flex justify-content-between align-items-center pdtop">
									<span><i class="bi-gift-fill"></i> Dhuro</span>
								</div>
							</div>
						</div></a>
					</div>
				</div>
						<?php
								global $wpdb;
								$sql2 = $wpdb->prepare("Select SUBQUERY.pt,SUBQUERY.ct from (select post_author as pt, count(*) as ct
															from wp_posts
															WHERE post_type='postimet'
															group by post_author) AS SUBQUERY
															ORDER BY SUBQUERY.ct DESC LIMIT 1 OFFSET 2");
								$res2 = $wpdb->get_results( $sql2 );
								foreach($res2 as $wp_ranking2){}
						?>
				<div class="col-sm-4 hoverE">
					<div class="leaderboard-card">
						<div class="leaderboard-card__top">
							<h3 class="text-center"><strong><?php echo $wp_ranking2->ct ;?></strong></h3>
						</div>
					<a href="<?=get_author_posts_url($wp_ranking2->pt)?>" class="rmlinkStyle">
						<div class="leaderboard-card__body">
							<div class="text-center">
								<img src="<?php echo get_avatar_url($id_or_email=$wp_ranking2->pt, 30);?>" class="circle-img mb-2" alt="User Img">
								<h5 class="mb-0"><strong><?php echo get_the_author_meta( 'first_name', $wp_ranking2->pt ); ?>&nbsp;<?php echo get_the_author_meta( 'last_name', $wp_ranking2->pt ); ?></strong></h5>
								<p class="text-muted mb-0"><i class="bi bi-envelope-fill">&nbsp;</i><?php echo get_the_author_meta( 'email', $wp_ranking2->pt ); ?></p>
								<hr>
								<div class="d-flex justify-content-between align-items-center pdtop">
									<span><i class="bi-gift-fill"></i> Dhuro</span>
								</div>
							</div>
						</div></a>
					</div>
				</div>
			</div>
         </div>
      </div>
      <!-- end best -->
     
      <!-- two_box section -->
      <div  class="two_box gradAnimation">
         <div class="container-fluid ">
            <div class="row d_flex">
               <div class="col-md-6">
                  <div class="two_box_img">
                     <figure><img src="images/leptop.jpg" alt="#"/></figure>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="two_box_img">
                     <h2><span class="offer">65% </span>Kategoria Femra</h2>
                     <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end two_box section -->
	
	 <?php get_footer(); ?>