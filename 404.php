<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package dhuro
 */

get_header();
?>
</br></br>
	<main id="primary" class="site-main" >
<div class="container-fluid">
				 <div style="float:center;text-align:center;">
				   <div style="margin-bottom:12%;margin:flex;">
					 <img src="<?php echo get_template_directory_uri() . '/assets/images/404.png';?>"/>
					 
					 <h5><strong>Faqja nuk u gjetë !</strong></h5>
				   </div>
				   <?php
					get_search_form();
					?>
				 </div>
			</div>
	</main><!-- #main -->
</br></br></br>
<?php
get_footer();
