<?php

get_header();
?>
	<main id="primary" class="site-main">
		<div class="site-main3">
			<header class="page-header">
				<h1 class="page-title">
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Rezultatet e kerkimit për: %s', 'dhuro' ), '<span>' . get_search_query() . '</span>' );
					?>
				</h1>
			</header><!-- .page-header -->
				<div class="row">
					
					<div class="col-xs-12 col-sm-8">
						
						<div class="row">

						<?php 
						
						if( have_posts() ):
							
							while( have_posts() ): the_post(); ?>
								
								<?php get_template_part('/template-parts/content', 'search'); ?>
							
							<?php endwhile;
							
						else :

							get_template_part( 'template-parts/content', 'none' );
							
						endif;
								
						?>
						</div>
					
					</div>
					
					<div class="col-xs-12 col-sm-4">
						<?php get_sidebar(); ?>
					</div>
					
					<?php the_posts_pagination();?>
				</div>
		</div>
	</main><!-- #main -->

<?php
get_footer();
