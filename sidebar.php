<?php


if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<aside id="secondary" class="widget-area">
</br>
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
<h2 class="ark">Kategoritë</h2><hr>
	<?php
		$categories = get_categories( array(
			'taxonomy' => 'kategorite',
			'orderby' => 'name',
			'order'   => 'ASC'
		) );
		foreach( $categories as $category ):
			$category_link = sprintf( 
				'<a href="%1$s" alt="%2$s">%3$s</a>',
				esc_url( get_category_link( $category->term_id ) ),
				esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
				esc_html( $category->name )
			);?>
			<div class="kategorit">
			<a href="<?php echo get_category_link( $category->term_id );?>"><img class="im1" src="<?php echo get_template_directory_uri() . '/assets/images/' . $category->name . '.png'?>"/><span class="cname"><?php echo $category->name;?></span></a>
				<?php
				echo '<p class="cnumri">' . sprintf( $category->count . ' Postime') . '</p> ';
				?>
			</div>
		<?php endforeach; ?>

</br>
</aside><!-- #secondary -->
