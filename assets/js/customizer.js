/* global wp, jQuery */
/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
  $(function(){
    $('.mydropdownjQuery').hover(function() {
        $(this).addClass('open');
    }
    );
});
 */
 /* social orbit links*/
 $("#gmail-orbit").click(function() {
  window.location.href = 'https://gmail.com'
});
$("#discord-orbit").click(function() {
  window.location.href = 'https://discord.com'
});
$("#twiter-orbit").click(function() {
  window.location.href = 'https://twitter.com'
});
$("#github").click(function() {
  window.location.href = 'https://github.com'
});
 
$(".dropdown").on("hide.bs.collapse", function() {
  $(this).find(".dropdown-menu").first().addClass( "sliding" )
});
$(".dropdown").on("hidden.bs.collapse", function() {
  $(this).find(".dropdown-menu").first().removeClass( "sliding" )
});
$(document).click(function() {
  $(".dropdown-menu.collapse.show").collapse("hide");
});
 
$(window).resize(function(){document.getElementById('nav2').setAttribute('style', 'padding-top:4px !important;');});
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
   if (prevScrollpos >= currentScrollPos && window.matchMedia("(max-width:  768px)").matches) {
    document.getElementById('nav2').style.top = "";
	document.getElementById('nav2').setAttribute('style', 'margin-top: 0rem !important;');
  } else if(1 <= currentScrollPos && window.matchMedia("(max-width:  768px)").matches) {
	document.getElementById('nav2').setAttribute('style', 'margin-top: 0rem !important;top: -400px !important;');
  }
  else{document.getElementById('nav2').setAttribute('style', 'padding-top:4px !important;');}
  prevScrollpos = currentScrollPos;
}
document.addEventListener('mouseup', function(e) {
    var container = document.getElementById('shto');
    if (!container.contains(e.target)) {
        container.style.opacity = '0';
		container.style.pointerEvents = 'none';
    }
});


//Get the button
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}

function getElementTopLeft(id) {

    var ele = document.getElementById(id);
    var top = 0;
    var left = 0;
   
    while(ele.tagName != "BODY") {
        top += ele.offsetTop;
        left += ele.offsetLeft;
        ele = ele.offsetParent;
    }
   
    return { top: top, left: left };
   
}
// When the user clicks on div, open the popup
function toggle_visibility(shto) {
	
	var pos = $('.b').offset();
	var top = 50;
	var left = pos.left - 1050 ;
	var l2 = 180;
	if(left>=-50){
	}
	else if(-50>left && left>=-300){
		l2 =l2 + 40;
		left = -280;}
	else if(-300>left){
		left = -280;
		l2 =l2 + 40;
		}
	$('.info').css({
	  position:'absolute',
	  top:top+'px',
	  left:left+'px',
	  right:l2+'px',
	});
	
	var e = document.getElementById(shto);
	
	if(e.style.opacity == '1'){
	e.style.opacity = '0';}
	else{
		
	   e.style.opacity = '1';
	   e.style.display ='block';
	   e.style.pointerEvents = 'auto';
	   }
	   
 }

 /*CountDown Timer Script*/	
	//var countDownDate = document.getElementById("appCountDown").getAttribute("data-value");
	// Set the date we're counting down to
	$(document).ready(function(){
    $(".autoc").trigger('click'); 
});


function timerh(element,x,dhuruar){
	
var countDownDate = new Date(x).getTime();
		
// Update the count down every 1 second
var x1 = setInterval(function(cd = countDownDate) {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = cd - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById(element).innerHTML = "Free&nbsp;";
  if (1< distance && distance <= 70000 && dhuruar == 1) {
	  document.getElementById(element).classList.add('abouttoExpire');
	  document.getElementById(element).classList.remove('abouttoExpire2');
    document.getElementById(element).innerHTML = "<i class='bi bi-stopwatch-fill'></i>&nbsp;" + days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";} 
  if (70000< distance && distance <= 1800000 && dhuruar == 1) {
	  document.getElementById(element).classList.add('abouttoExpire2');
	  document.getElementById(element).classList.remove('abouttoExpire');
    document.getElementById(element).innerHTML = "<i class='bi bi-stopwatch-fill'></i>&nbsp;" + days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";} 
  
  if(1800000 < distance && dhuruar == 1){
  document.getElementById(element).innerHTML = "<i class='bi bi-stopwatch-fill'></i>&nbsp;" + days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";} 
    
  // If the count down is over, write some text 
  if (dhuruar == 2) {
	   document.getElementById(element).classList.add('dhuruarColor');
    document.getElementById(element).innerHTML = "Dhuruar";
  }
    
  if (distance < 0 && dhuruar == 1) {
    clearInterval(x1);
	
	document.location.href ="/dhuro/profili/?i="+element+"&d=2"
	
    document.getElementById(element).innerHTML = "<i class='bi bi-stopwatch-fill'></i>&nbsp;" +"Përfundoi";
  }
}, 1000);
}




