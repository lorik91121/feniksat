<?php
        /* 

	Template Name:  Postimet Page

	*/

        get_header();
        if (is_user_logged_in() && is_front_page()) {


        ?>

          <div class="container">
              <div class="page1">
                  <div class="left-side1 shadow" style="position:sticky; top:101px; margin-bottom:20px;">

                  <h3 style="text-align:center;">Aktivitetet e fundit</h3>
                  <?php 

                $args = array(  
                'post_type' => 'postimet',
                'post_status' => 'publish',
                'posts_per_page' => 5, 
                'cat' => 'femra', 'meshkuj', 'femije',
                );

                $lastBlog = new WP_Query( $args ); 
                if ($lastBlog->have_posts()): 
                while ($lastBlog->have_posts()): $lastBlog->the_post();  ?>
                                <?php get_template_part('post',get_post_format()); 
                endwhile;
                endif;

                wp_reset_postdata();

                ?>

                  </div>

                  <div class="content1">
                      <div class="panel profile-cover">
                          <div class="panel">
                              <div class="panel-heading">
                                  <h3 class="panel-title">Aktivitetet</h3>
                              </div>
                              <div class="panel-content panel-activity">

                                  <div class="post-top-bar">
                                      <!--  <p><a href="#" class="name"><?php //if (is_user_logged_in()) {$user = wp_get_current_user(); echo $user->display_name;}
                                                                        ?></a> likes this</p>-->

                                      <div class="column two mobileA">
                                          <?php if (!is_user_logged_in()) { ?>
                                              <article class="moodle-container">
                                                  <div class="share-container" href="#myModal2" data-toggle="modal">

                                                      <h4 style="padding-top:2%; margin-left:5px;"><b>Shkruaj nje artikull ose
                                                              shperndaje nje foto</b></h4>
                                                  </div>
                                                  <div class=" action-container" href="#post" data-toggle="modal">
                                                      <br>
                                                      <button class="post-button">Posto</button>
                                                  </div>

                                              </article>

                                          <?php } else {
                                            ?>
                                              <article class="moodle-container">
                                                  <div class="share-container" href="#post" data-toggle="modal">
                                                      <img class="foto" style="padding:4px;height:60px;width:60px;" <?php if (is_user_logged_in()) {
                                                                                                                        $user = wp_get_current_user();
                                                                                                                        echo get_avatar($id_or_email = $user->ID, 60);
                                                                                                                    } ?></img>
                                                      <h4 style="padding-top:2%; margin-left:5px;"><b>Shkruaj nje artikull ose
                                                              shperndaje nje foto</b></h4>
                                                  </div>
                                                  <div class=" action-container" href="#post" data-toggle="modal">
                                                      <br>
                                                      <button class="post-button" style="background-color:#1E6F5C; border:#74b39c;">Posto</button>
                                                  </div>
                                              </article>
                                              <div class="panel-heading">
                                                  <h3 class="panel-title" style="color:#FF9F45;">Te gjitha postimet</h3>
                                                  <?php get_search_form(); ?>
                                              </div>
                                          <?php
                                            } ?>
                                          <?php
                                            $query = new WP_Query(array(
                                                'post_type' => 'postimet',
                                                'post_status' => 'publish'
                                            ));


                                            while ($query->have_posts()) {
                                                $query->the_post();
                                                $post_id = get_the_ID();
                                                get_template_part('aktivitetet', get_post_format());
                                            }

                                            wp_reset_query();
                                            ?>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="d-flex flex-column right-side-main">
                      <div class="buton1 shadow">
                     
                  
                
                  <?php get_sidebar(); ?>
            </div>

                      <div class="right-side1 shadow" style="position:sticky; top:100px;">
                          <h4 class="card-title text-center"><strong> Perdoruesit e fundit</strong> </h4>
                          <div class="profile_info mx-5">
                              <?php

                                $user_array = array('number' => $show, 'orderby' => 'post_count', 'order' => 'DESC');
                                $users = get_users($user_array);
                                foreach ($users as $index => $user) :
                                    if ($index <= 5) :
                                ?>
                                      <a href="<?= get_author_posts_url($user->ID) ?>" style="text-decoration: none;">
                                          <?php echo get_avatar($user->ID, 65); ?>
                                      </a>
                              <?php
                                    endif;
                                endforeach;
                                ?>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <!--Posto-->
          <div id="post" class="modal fade" tabindex="-1">
              <div class="modal-dialog modal-md modal-dialog-centered ">
                  <div class="modal-content modal-post">
                      <!-- The modal close x-->
                      <div class="modal-header costumModalHeader">
                          <h5 class="modal-title" id="exampleModalLabel"></h5>
                          <div class="panel-heading">
                              <h3 class="panel-title">Krijo nje post te ri</h3>

                          </div>
                          <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>

                      </div>

                      <div class="modal-body mobileL2">
                          <form class="postoForm" method="post" id="postoForma" enctype="multipart/form-data">
                              <div class="d-flex flex-column justify-content-center align-items-center m-5 postoForm ">
                                  <!-- <div class="input-control">   -->

                                  <p> <input class="w3-input w3-border w3-round-xxlarge" name="titulli" id="titulli" placeholder="Titulli"></p>
                                  <!-- <input class="mb-auto" type="text" name="titulli" id="titulli" placeholder="Titulli"> -->
                                  <!-- <div class="error"></div>
                        </div>     -->
                                  <div class="d-flex flex-column">

                                      <select name="category" id="kategoria">
                                          <option value="" disabled selected>Zgjedh kategorine</option>
                                          <?php
                                            $args = array(
                                                'taxonomy' => 'kategorite',
                                                'orderby' => 'name',
                                                'order'   => 'ASC'
                                            );

                                            $cats = get_categories($args);

                                            foreach ($cats as $cat) {
                                            ?>
                                              <option value="<?php echo $cat->name; ?>"><?php echo $cat->name; ?></option>
                                          <?php
                                            }
                                            ?>
                                      </select>
                                      </select>
                                      <!-- <div class="error"></div>
                                  </div> -->
                                  </div>

                                  <label class="custom-file-upload">
                                      <!-- <div class="input-control"> -->
                                      <input type="file" name="image" id="image" />

                                      <i class="bi bi-upload"></i> Shto nje foto

                                      <!-- <div class="error"></div>
                                    </div> -->
                                  </label>


                                  <div id="preview"></div>

                                  <!-- <div class="input-control"> -->
                                  <textarea class="mb-auto" name="description" placeholder="Trego me teper per artikullin..." id="pershkrimi"></textarea>
                                  <!-- <div class="error"></div>  
                            </div> -->

                                  <button type="submit" class="submit-posto" id="butoni"><?php _e('Posto', 'mytextdomain') ?></button>



                                  <input type="hidden" name="post_type" id="post_type" value="my_custom_post_type" />


                                  <?php wp_nonce_field('cpt_nonce_action', 'cpt_nonce_field'); ?>

                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
          <!---->
          <?php
            if (
                isset($_POST['cpt_nonce_field'])

                && wp_verify_nonce($_POST['cpt_nonce_field'], 'cpt_nonce_action')
            ) {

                $categories = array($_POST['category']);

                $my_cptpost_args = array(

                    'post_title'     => $_POST['titulli'],

                    'post_content'  => $_POST['description'],

                    'post_status'   => 'publish',

                    'post_type' => 'postimet'

                );



                $cpt_id = wp_insert_post($my_cptpost_args, false, true);

                wp_set_object_terms($cpt_id, $categories, 'kategorite');

                $upload = wp_upload_bits($_FILES["image"]["name"], null, file_get_contents($_FILES["image"]["tmp_name"]));

                if (!$upload_file['error']) {
                    $filename = $upload['file'];
                    $wp_filetype = wp_check_filetype($filename, null);
                    $attachment = array(
                        'post_mime_type' => $wp_filetype['type'],
                        'post_title' => sanitize_file_name($filename),
                        'post_content' => '',
                        'post_status' => 'inherit'
                    );

                    $attachment_id = wp_insert_attachment($attachment, $filename, $cpt_id);

                    if (!is_wp_error($attachment_id)) {
                        require_once(ABSPATH . 'wp-admin/includes/image.php');

                        $attachment_data = wp_generate_attachment_metadata($attachment_id, $filename);
                        wp_update_attachment_metadata($attachment_id, $attachment_data);
                        set_post_thumbnail($cpt_id, $attachment_id);
                    }
                }
            }
        } else {
            ?>
          <!-- header -->
          <header>
              <!-- header inner -->
              <div class="head_top">

                  <!-- end header inner -->
                  <!-- end header -->
                  <!-- banner -->
                  <section class="banner_main">
                      <div class="container-fluid rmPadding">
                          <div class="row d_flex">
                              <div class="col-md-5">
                                  <div class="text-bg">
                                      <h1>FONDACIONI </br>DHURO</h1>
                                      <strong>DHURO CKA NUK TE DUHET, MERR CKA TE DUHET!</strong>
                                      <span>Dhuro Page 2022</span>
                                      <a href="#myModal2" data-toggle="modal">Dhuro tani</a>
                                  </div>
                              </div>
                              <div class="col-md-7 padding_right1">
                                  <div class="text-img gradAnimation">
                                      <img src="<?php echo get_template_directory_uri() . '/assets/images/headerPic2.png'; ?>" alt="#" />
                                      <h3>🖤</h3>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </section>
              </div>
          </header>
          <!-- end banner -->
          <!-- about -->
          <div id="about" class="about">
              <div class="container">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="titlepage">
                              <h2>Rreth Dhuro</h2>
                              <span>Ideja e kesaj webfaqe eshte qe personat te cilet kane nevoje te ‘pastrojne’ garderoben e tyre, nese nuk duan te shesin ato, por te dhurojne, permes kesaj webfaqe eshte e mundur ta bejne ate, duke nenkuptuar se nje perdorues tjeter i webfaqes tone do te mund te marre ate se cka dhurohet.Ne kemi bere te mundur qe cdokush te mund te regjistrohet ne webfaqen tone, pa pagese!</span>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-8 offset-md-2 ">
                          <div class="about_box">
                              <a class="text-bg2" href="<?php echo home_url() . '/rethe-nesh/'; ?>">Më shumë</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- about -->
          <div class="container-fluid orbit">
              <img class="body2" id="sun" src="<?php echo get_template_directory_uri() . '/assets/images/dhuro-logo (2).png' ?>"></img>

              <div class="body2" id="gmail-orbit">
                  <img class="body2" id="gmail" src="<?php echo get_template_directory_uri() . '/assets/images/gmail.png' ?>" />
              </div>

              <div class="body2" id="discord-orbit">
                  <img class="body2" id="discord" src="<?php echo get_template_directory_uri() . '/assets/images/Discord Logo Circle.png' ?>" />
              </div>

              <div class="body2" id="twiter-orbit">
                  <img class="body2" id="twiter" src="<?php echo get_template_directory_uri() . '/assets/images/twiter.png' ?>" />
              </div>


              <div class="body2" id="github-orbit">
                  <img class="body2" id="github" src="<?php echo get_template_directory_uri() . '/assets/images/github.png' ?>" />
              </div>
          </div>
          <!-- best -->
          <div id="" class="best">
              <div class="container">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="titlepage">
                              <h2>Dhuruesit më aktiv</h2>
                              <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</span>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-4 hoverE">
                          <div class="leaderboard-card">
                              <div class="leaderboard-card__top">
                                  <?php
                                    global $wpdb;
                                    $sql = $wpdb->prepare("Select SUBQUERY.pt,SUBQUERY.ct from (select post_author as pt, count(*) as ct
															from wp_posts
															WHERE post_type='postimet'
															group by post_author) AS SUBQUERY
															ORDER BY SUBQUERY.ct DESC LIMIT 1 OFFSET 1");
                                    $res = $wpdb->get_results($sql);
                                    foreach ($res as $wp_ranking) {
                                    }
                                    ?>
                                  <h3 class="text-center"><strong><?php echo $wp_ranking->ct; ?></strong></h3>
                              </div>
                              <a href="<?= get_author_posts_url($wp_ranking->pt) ?>" class="rmlinkStyle">
                                  <div class="leaderboard-card__body">
                                      <div class="text-center">
                                          <img src="<?php echo get_avatar_url($id_or_email = $wp_ranking->pt, 30); ?>" class="circle-img mb-2" alt="User Img">
                                          <h5 class="mb-0"><strong><?php echo get_the_author_meta('first_name', $wp_ranking->pt); ?>&nbsp;<?php echo get_the_author_meta('last_name', $wp_ranking->pt); ?></strong></h5>
                                          <p class="text-muted mb-0"><i class="bi bi-envelope-fill"></i>&nbsp;<?php echo get_the_author_meta('email', $wp_ranking->pt); ?></p>
                                          <hr>
                                          <div class="d-flex justify-content-between align-items-center pdtop">
                                              <span><i class="bi-gift-fill"></i> Dhuro</span>
                                          </div>
                                      </div>
                                  </div>
                              </a>
                          </div>
                      </div>
                      <?php
                        global $wpdb;
                        $sql1 = $wpdb->prepare("Select SUBQUERY.pt,SUBQUERY.ct from (select post_author as pt, count(*) as ct
															from wp_posts
															WHERE post_type='postimet'
															group by post_author) AS SUBQUERY
															ORDER BY SUBQUERY.ct DESC LIMIT 1 OFFSET 0");
                        $res1 = $wpdb->get_results($sql1);
                        foreach ($res1 as $wp_ranking1) {
                        }
                        ?>
                      <div class="col-sm-4 hoverE">
                          <div class="leaderboard-card leaderboard-card--first">
                              <div class="leaderboard-card__top">
                                  <h3 class="text-center"><strong><?php echo $wp_ranking1->ct; ?></strong></h3>
                              </div>
                              <a href="<?= get_author_posts_url($wp_ranking1->pt) ?>" class="rmlinkStyle">
                                  <div class="leaderboard-card__body">
                                      <div class="text-center">
                                          <img src="<?php echo get_avatar_url($id_or_email = $wp_ranking1->pt, 30); ?>" class="circle-img mb-2" alt="User Img">
                                          <h5 class="mb-0"><strong><?php echo get_the_author_meta('first_name', $wp_ranking1->pt); ?>&nbsp;<?php echo get_the_author_meta('last_name', $wp_ranking1->pt); ?></strong></h5>
                                          <p class="text-muted mb-0"><i class="bi bi-envelope-fill"></i>&nbsp;<?php echo get_the_author_meta('email', $wp_ranking1->pt); ?></p>
                                          <hr>
                                          <div class="d-flex justify-content-between align-items-center pdtop">
                                              <span><i class="bi-gift-fill"></i> Dhuro</span>
                                          </div>
                                      </div>
                                  </div>
                              </a>
                          </div>
                      </div>
                      <?php
                        global $wpdb;
                        $sql2 = $wpdb->prepare("Select SUBQUERY.pt,SUBQUERY.ct from (select post_author as pt, count(*) as ct
															from wp_posts
															WHERE post_type='postimet'
															group by post_author) AS SUBQUERY
															ORDER BY SUBQUERY.ct DESC LIMIT 1 OFFSET 2");
                        $res2 = $wpdb->get_results($sql2);
                        foreach ($res2 as $wp_ranking2) {
                        }
                        ?>
                      <div class="col-sm-4 hoverE">
                          <div class="leaderboard-card">
                              <div class="leaderboard-card__top">
                                  <h3 class="text-center"><strong><?php echo $wp_ranking2->ct; ?></strong></h3>
                              </div>
                              <a href="<?= get_author_posts_url($wp_ranking2->pt) ?>" class="rmlinkStyle">
                                  <div class="leaderboard-card__body">
                                      <div class="text-center">
                                          <img src="<?php echo get_avatar_url($id_or_email = $wp_ranking2->pt, 30); ?>" class="circle-img mb-2" alt="User Img">
                                          <h5 class="mb-0"><strong><?php echo get_the_author_meta('first_name', $wp_ranking2->pt); ?>&nbsp;<?php echo get_the_author_meta('last_name', $wp_ranking2->pt); ?></strong></h5>
                                          <p class="text-muted mb-0"><i class="bi bi-envelope-fill">&nbsp;</i><?php echo get_the_author_meta('email', $wp_ranking2->pt); ?></p>
                                          <hr>
                                          <div class="d-flex justify-content-between align-items-center pdtop">
                                              <span><i class="bi-gift-fill"></i> Dhuro</span>
                                          </div>
                                      </div>
                                  </div>
                              </a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- end best -->

          <!-- two_box section -->
          <div class="two_box gradAnimation">
              <div class="container-fluid ">
                  <div class="row d_flex">
                      <div class="col-md-6">
                          <div class="two_box_img">
                              <figure><img src="images/leptop.jpg" alt="#" /></figure>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="two_box_img">
                              <h2><span class="offer">65% </span>Kategoria Femra</h2>
                              <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable</p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- end two_box section -->

      <?php
        }


        ?>

      <?php get_footer(); ?>