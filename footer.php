<?php if(!is_front_page()) : ?>
<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
    <?php if(is_active_sidebar('sidebar')): ?>
    <?php dynamic_sidebar('sidebar'); ?>
    <?php endif; ?>
</div><!-- /.blog-sidebar -->
<?php endif; ?>
</div><!-- /.row -->


</div><!-- /.container -->
<footer class="bg-light text-center text-md-start" style="border: 1px solid #e7e7e7;">
    <!-- Copyright -->
    <div class="foterlogo d-flex justify-content-center">
        <div class="butonlogo"><?php the_custom_logo(); ?></div>
        
    </div>
    <!--------------------------- Footer widges ---------------------------------------->

    <div class="footer-sidebar d-flex flex-row align-items-center my-5">
         
        <div class="kategorit12">
             <h3 class="kagtegorija ">Katergorit</h3> 
            <?php
		$categories = get_categories( array(
			'taxonomy' => 'kategorite',
			'orderby' => 'name',
			'order'   => 'ASC'
		) );
		foreach( $categories as $category ):
			$category_link = sprintf( 
				'<a href="%1$s" alt="%2$s">%3$s</a>',
				esc_url( get_category_link( $category->term_id ) ),
				esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
				esc_html( $category->name )
			);?>
            <div class="kategorit4">
                <a href="<?php echo get_category_link( $category->term_id );?>"><img class="im15"
                        src="<?php echo get_template_directory_uri() . '/assets/images/' . $category->name . '.png'?>" />
                    <span class="cname4"><?php echo $category->name;?></span></a>
                <?php
				echo '<p class="cnumri">' . sprintf( $category->count . ' Postime') . '</p> ';
				?>
            </div>
            <?php endforeach; ?>
        </div>
        
        <div class="arkiva3 ">
            <h2>Arkiva</h2>
            <hr>
            <?php  
		global $wpdb;  $limit = 0;  
		$year_prev = null;
		$months = $wpdb->get_results("SELECT DISTINCT MONTH( post_date ) AS month ,	YEAR( post_date ) AS year, COUNT( id ) as post_count FROM $wpdb->posts WHERE post_status = 'publish' and post_date <= now( ) and post_type = 'Postimet' GROUP BY month , year ORDER BY post_date ASC"); 
			foreach($months as $month) :  	
				$year_current = $month->year;  	
					if ($year_current != $year_prev){  		
						if ($year_prev != null){?><?php } ?>
            <li class="archive-year list-unstyled"><img id="arkivaImg"
                    src="<?php echo get_template_directory_uri() . '/assets/images/calendar.svg';?>" /><a
                    href="<?php bloginfo('url') ?>/<?php echo $month->year; ?>/?post_type=postimet"><?php echo $month->year; ?></a>
            </li> <?php } ?> <li class="d-flex muaji"><a
                    href="<?php bloginfo('url') ?>/<?php echo $month->year; ?>/<?php echo date("m", mktime(0, 0, 0, $month->month, 1, $month->year)) ?>?post_type=postimet"><span
                        class="archive-month"><?php echo date_i18n("F", mktime(0, 0, 0, $month->month, 1, $month->year)) ?><span
                            class="numri"><?php echo '['.$month->post_count.']'; ?></span></span></a></li>
            <?php $year_prev = $year_current;    
					if(++$limit >= 18) { break; }    
			endforeach; ?>
        </div>

        <div class="listat">

            <ul class="foterlist ">
                <li>
                    <h4><i class='fas fa-info-circle   '></i><a href="http://localhost/dhuro/rreth-nesh/">Rreth nesh </a></h4>
                </li>
                <hr>
                <li>
                    <h4><i class='fas fa-phone-square-alt '></i><a href="http://localhost/dhuro/kontakt/">Na kontakto</a></h4>
                </li>

            </ul>
        </div>
         
    </div>

    <!--------------------------- Footer widges ----------------------------------->
    <div class="text-right p-3" style="background-color: #f8f8f8;">
            <Button class="backbtn1 btn btn-sm" onclick="topFunction()" id="myBtn" style="background-color: #1E6F5C;">Back to top ⬆️</Button>
        </div>

    <div class="text-center p-3" style="background-color:#f8f8f8">
        © 2022 Copyright:
        <a class="text-dark" href="#">Dhuro.com</a>
    </div>
       

    <!-- Copyright -->


</footer>
 

</body>
<?php wp_footer(); ?>
</html>