<?php /* 

	Template Name:  Profile Page

	*/
	get_header(); 

  global $current_user, $wp_roles;

$error = array();    
/* If profile was saved, update profile. */
if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {

    /* Update user password. */
    if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {

        if ( $_POST['pass1'] == $_POST['pass2'] )
    
            wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass2'] ) ) );
            
        else
            $error[] = __('The passwords you entered do not match.  Your password was not updated.', 'profile');
    }

    /* Update user information. */
    if ( !empty( $_POST['url'] ) )
        wp_update_user( array( 'ID' => $current_user->ID, 'user_url' => esc_url( $_POST['url'] ) ) );
    if ( !empty( $_POST['email'] ) ){
        if (!is_email(esc_attr( $_POST['email'] )))
            $error[] = __('The Email you entered is not valid.  please try again.', 'profile');
        elseif(email_exists(esc_attr( $_POST['email'] )) != $current_user->id )
            $error[] = __('This email is already used by another user.  try a different one.', 'profile');
        else{
            wp_update_user( array ('ID' => $current_user->ID, 'user_email' => esc_attr( $_POST['email'] )));
        }
    }

    if ( !empty( $_POST['first-name'] ) )
        update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first-name'] ) );
    if ( !empty( $_POST['last-name'] ) )
        update_user_meta($current_user->ID, 'last_name', esc_attr( $_POST['last-name'] ) );
    if ( !empty( $_POST['description'] ) )
        update_user_meta( $current_user->ID, 'description', esc_attr( $_POST['description'] ) );

    /* Redirect so the page will show updated info.*/
  /*I am not Author of this Code- i dont know why but it worked for me after changing below line to if ( count($error) == 0 ){ */
    if ( count($error) == 0 ) {
        //action hook for plugins and extra fields saving
        do_action('edit_user_profile_update', $current_user->ID);
        wp_redirect( get_permalink() );
        exit;
    }
}?>
  
<div class="containerProfile">
  <div class="profileheader" >
  
   

    </div>
    
  </div>

  <div class="main-bd">
    <div class="left-side">
      <div class="profile-side" style="position:sticky; top:101px;"> 
      <div class="profile-img">
		  <a href="#myModal" data-toggle="modal" id="chatBtn">
		  <i class="fas fa-plus-circle fa-2x"></i>
			<img style="margin-top:7%;" src="<?php if (is_user_logged_in()) {$user = wp_get_current_user(); echo get_avatar_url($id_or_email=$user->ID, 60);}?>"/>
		  </a>
      </div>
      <h3 class="userName" style="text-align:center;"><?php if (is_user_logged_in()) {$user = wp_get_current_user(); echo $user->display_name;}?></h3>
        <p class="mobile-no" style="text-align:center;"><i class="fa fa-phone"></i><?php if (is_user_logged_in()) {$user = wp_get_current_user(); echo $user->user_registered;}?></p>
        <p class="user-mail"><i class="fa fa-envelope"></i> <?php if (is_user_logged_in()) {$user = wp_get_current_user(); echo $user->user_email;}?></p>
      
        <div class="profile-btn">
          <button class="chatbtn" href="#edit" data-toggle="modal" id="chatBtn"><i class="fa fa-comment"></i>Ndrysho Fjalekalimin</button>
        </div>

      </div>

    </div>


    <div class="right-side">
    <br><br>
      <div class="userdata">
        <ul>
          <li onclick="tabs(0)" class="user-post active">Profili im</li>
          <li onclick="tabs(1)" class="user-review">Postimet</li>
       
        </ul>
      </div>

      <div class="profile-body">
        <div class="profile-posts tab">
          <h1>Te dhenat</h1>
   
            <table class="table">
  <thead>
    <tr>
      
      <th scope="col">Emri:  </th>
      <th scope="col"><?php if (is_user_logged_in()) {$user = wp_get_current_user(); echo $user->display_name;}?></th>
      
    </tr>
  </thead>
  <tbody>
    <tr>
  
    <th scope="col">Mbiemri:  </th>
      <th scope="col"><?php if (is_user_logged_in()) {$user = wp_get_current_user(); echo $user-> user_nicename;}?></th>
     
    </tr>
    <tr>
  
  <th scope="col">Email Adresa:  </th>
    <th scope="col"><?php if (is_user_logged_in()) {$user = wp_get_current_user(); echo $user-> user_email;}?></th>
   
  </tr>
  </tbody>
</table>
        </div>
        <div class="profile-reviews tab">
          <h1>Postimet e mija</h1>
          <div class="post-top-bar">
      <!--  <p><a href="#" class="name"><?php //if (is_user_logged_in()) {$user = wp_get_current_user(); echo $user->display_name;}?></a> likes this</p>-->
      
  <div class="">
  <div class="post-container-profile">

  <?php
  global $current_user;
  get_currentuserinfo();
  $authorID = $current_user->ID;
                  $query = new WP_Query(array(
                    'post_type' => 'postimet',
                    'post_status' => 'publish',
                    'author' => $authorID
                ));
                
                
                while ($query->have_posts()) {
                    $query->the_post();
                    $post_id = get_the_ID();
                    get_template_part('userPost',get_post_format());
                }
                
                wp_reset_query();         
                ?>
          </div>
  </div>
 
</div>
     
     


        
      </div>
    </div>
  </div>
</div>
<!--START-->
   <div class="modaltopM modal fade" id="myModal" role="dialog">
		 <div class="modal-dialog">
		 <!-- Modal content-->
		 <div class="modal-content">
		 <div class="modal-header">
       
		 <h4 class="modal-title">Ndrysho Profilin</h4>
     <h5 class="modal-title" id="exampleModalLabel"></h5>
											<button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
     
		 </div>
     
		 <div class="modal-body">
			<?php add_thickbox();
			wp_enqueue_script( 'media-upload' );
			wp_enqueue_media();
			ayecode_admin_media_scripts();?>
				<table class="form-table ayecode-avatar-upload-options">
					<tr>
						<th>
							<label for="image"><?php _e('Imazhi:&nbsp;', 'ayecode'); ?></label>
						</th>
						<td>
							<?php
							// Check whether we saved the custom avatar, else return the default avatar.
							$custom_avatar = get_the_author_meta( 'ayecode-custom-avatar', $profileuser->ID );
							if ( $custom_avatar == '' ){
								$custom_avatar = get_avatar_url( $profileuser->ID );
							}else{
								$custom_avatar = esc_url_raw( $custom_avatar );
							}
							?>
						<form class="form-inline" name="form1" method="post" action="<?php echo get_home_url() . '/profili';?>" >	
						<img style="width: 96px; height: 96px; display: block; margin-bottom: 15px;" class="custom-avatar-preview" src="<?php echo $custom_avatar; ?>">
							<input readonly type="text" name="ayecode-custom-avatar" id="ayecode-custom-avatar" value="<?php echo esc_attr( esc_url_raw( get_the_author_meta( 'ayecode-custom-avatar', $profileuser->ID ) ) ); ?>" class="form-control regular-text" />
							<input type='button' class="search-submit avatar-image-upload button-primary" value="<?php esc_attr_e("Ngarko imazhin","ayecode");?>" id="uploadimage"/><br />
							<span class="description">
								<?php _e('Ju lutemi ngarkoni një imazh të personalizuar për profilin tuaj, për të hequr imazhin thjesht fshini URL-në dhe klikoni përditësimin.', 'ayecode'); ?>
							</span>
							<div class="modal-footer">
								<button type="submit" name="submit" value="Konfimo" class="btn btn-default">Konfirmo</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Anulo</button>
						</div>
						</form>
							
						</td>
					</tr>
				</table>
				<?php	
					global $current_user;
					$current_user =  wp_get_current_user();
					if ( current_user_can( 'edit_user', $current_user->ID ) ) {
						if( isset($_POST[ 'submit' ]) ){
							echo '<meta http-equiv="Refresh" content="0.1">';
							$avatar = esc_url_raw( $_POST[ 'ayecode-custom-avatar' ] );
							update_user_meta( $current_user->ID, 'ayecode-custom-avatar', $avatar );
							
							//echo '<script>alert("hello ' . get_home_url()  . '")</script>';
							
						}
					}
				?>
		 </div>
		 
		 </div>

		 </div>
 </div>
<!--END-->
    <!--START-->
    <div id="edit" class="modal fade" tabindex="-1">
    <div class="modal-dialog mobileL w-50 p-3 modal-lg">
								<div class="modal-content">
                    <!-- The modal close x-->
										<div class="modal-header costumModalHeader">
											<h5 class="modal-title" id="exampleModalLabel"></h5>
											<button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
             
									<div class="modal-body mobileL2">
               
            <div class="row">
    <div class="col-md-12">
  	<div class="form1">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>">
        <div class="entry-content entry">
            <?php the_content(); ?>
            <?php if ( !is_user_logged_in() ) : ?>
                    <p class="warning">
                        <?php _e('You must be logged in to edit your profile.', 'profile'); ?>
                    </p><!-- .warning -->
            <?php else : ?>
              
                <?php if ( count($error) > 0 ) echo '<p class="error">' . implode("<br />", $error) . '</p>'; ?>
                <form method="POST" id="adduser" action="<?php the_permalink(); ?>">
              
                      
                    <p class="form-username">
                        <label for="first-name"><?php _e('First Name', 'profile'); ?></label>
                        <input class="text-input" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>" />
                    </p><!-- .form-username -->
                    <p class="form-username">
                        <label for="last-name"><?php _e('Last Name', 'profile'); ?></label>
                        <input class="text-input" name="last-name" type="text" id="last-name" value="<?php the_author_meta( 'last_name', $current_user->ID ); ?>" />
                    </p><!-- .form-username -->
                    <p class="form-email">
                        <label for="email"><?php _e('E-mail *', 'profile'); ?></label>
                        <input class="text-input" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" />
                    </p><!-- .form-email -->
                
                    <p class="form-password">
                        <label for="pass1"><?php _e('Password *', 'profile'); ?> </label>
                        <input class="text-input" name="pass1" type="password" id="pass1" />
                    </p><!-- .form-password -->
                    <p class="form-password">
                        <label for="pass2"><?php _e('Repeat Password *', 'profile'); ?></label>
                        <input class="text-input" name="pass2" type="password" id="pass2" />
                    </p><!-- .form-password -->
                    <?php 
                        //action hook for plugin and extra fields
                       // do_action('edit_user_profile',$current_user); 
                    ?>
                    <p class="form-submit">
                        <?php echo $referer; ?>
                        <button class="btn"> Ruaj</button>
                        <?php wp_nonce_field( 'update-user' ) ?>
                        <input name="action" type="hidden" id="action" value="update-user" />
                    </p><!-- .form-submit -->
                </form><!-- #adduser -->
            <?php endif; ?>
        </div><!-- .entry-content -->
    </div><!-- .hentry .post -->
    <?php endwhile; ?>
<?php else: ?>
    <p class="no-data">
        <?php _e('ballina', 'profile'); ?>
    </p><!-- .no-data -->
<?php endif; ?>
										</div>
	</div>
</div>    

                  
</div>
</div>
</div>
</div>
<!--END-->

<!--  partial -->


  <script>
    $(".userdata ul li").click(function() {
  $(this)
    .addClass("active")
    .siblings()
    .removeClass("active");
});

const tabBtn = document.querySelectorAll(".userdata ul li");
const tab = document.querySelectorAll(".tab");

function tabs(panelIndex) {
  tab.forEach(function(node) {
    node.style.display = "none";
  });
  tab[panelIndex].style.display = "block";
}
tabs(0);

let bio = document.querySelector(".bio");
const bioMore = document.querySelector("#see-more-bio");
const bioLength = bio.innerText.length;

function bioText() {
  bio.oldText = bio.innerText;

  bio.innerText = bio.innerText.substring(0, 100) + "...";
  bio.innerHTML += `<span onclick='addLength()' id='see-more-bio'>See More</span>`;
}
//        console.log(bio.innerText)

bioText();

function addLength() {
  bio.innerText = bio.oldText;
  bio.innerHTML +=
    "&nbsp;" + `<span onclick='bioText()' id='see-less-bio'>See Less</span>`;
  document.getElementById("see-less-bio").addEventListener("click", () => {
    document.getElementById("see-less-bio").style.display = "none";
  });
}
if (document.querySelector(".alert-message").innerText > 9) {
  document.querySelector(".alert-message").style.fontSize = ".7rem";
}
    </script>


<?php 	get_footer(); 

	?>
