<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <label for="s" >
        <input type="search" style="margin-left: 95%; border-radius:15px; width:6vw; height:3vh; font-family:'bootstrap-icons', Arial" class="form-control "
            placeholder="&#xf52a; Kerko... "
            value="<?php echo get_search_query() ?>" name="s"
            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
           
    </label>
   
    <!-- <input type="submit" class="search-submit"
        value="<?php //echo esc_attr_x( 'Kërko', 'submit button' ) ?>" />
		 -->
</form>