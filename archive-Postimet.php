<?php

get_header();
?>
<?php 
global $wp_query;
	if (!empty($wp_query->query_vars['page'])) {
		$the_year_url = $wp_query->query_vars['page'];
		echo '<script>alert("The year is: ' . $the_year_url.'")</script>';
	}
?>
<main id="primary" class="site-main">
	<div class="site-main2">
		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header>

			<?php
		
			while ( have_posts() ) :
				the_post();
?>
			
				<?php get_template_part('template-parts/content','archive'); ?>

	<?php		endwhile;

			the_posts_pagination();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
	</div>
</main><!-- #main -->
This is a costum Archive Page
<?php
get_footer();
