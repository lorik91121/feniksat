<br><br>
<?php  get_header();  ?>

<div class="post-container-profile ">

    <div class="left-side1 shadow" style="position:sticky; top:101px; margin-bottom:20px;">

        <h3 style="text-align:center;">Postimet e fundit</h3>
        <?php

  $args = array(
      'post_type' => 'postimet',
      'post_status' => 'publish',
      'posts_per_page' => 3,
      'cat' => 'femra', 'meshkuj', 'femije',
  );

  $lastBlog = new WP_Query($args);
  if ($lastBlog->have_posts()) :
      while ($lastBlog->have_posts()) : $lastBlog->the_post();  ?>
        <?php get_template_part('post', get_post_format());
      endwhile;
  endif;

  wp_reset_postdata();

  ?>

    </div>

    <article class="postcomments">


        <div class="profile-title"><img src="<?php 
                                    $auth_id = $post->post_author; 
                                     print get_avatar_url(get_the_author_meta('ID', $auth_id)); 
                                ?>" />
            <div class="user-information-container">
                <p><a href="<?=get_author_posts_url($auth_id)?>" class="name"><?php
                                    $auth_id = $post->post_author; 
                                    echo get_the_author_meta( 'display_name', $auth_id ); 
                                    ?>
                    </a></p>
                <p class=""><?php echo '<span class="pub-date">'.get_the_date( 'j F, Y').'</span>';
                                    echo '<span class="pub-date"> '.get_the_time().'</span>';  ?></p>

            </div>
        </div>
        <div>
            <h4 class="teksti"><?php the_content();?></h4>
            <img src="<?php the_post_thumbnail_url();?>">

        </div>
        <div class="bottom-bar-container">
            <div class="bottom-bar-social">
                <input id="pelqeje" type="checkbox" placeholder="" name="socialIcon" />
                <label class="social" for="like" id="liked-after"><span class="glyphicon glyphicon-thumbs-up"></span>
                    Pelqeje</label>
                <input id="komento" type="checkbox" placeholder="" name="socialIcon" />
                <label class="social" for="comment">
                    <span class="glyphicon glyphicon-comment"></span> Komento</label>

                <div>
                    <hr>

                    <?php
 $args = array(
    'status' => 'approve',
    'post_id'=> $post->ID
);

// The comment Query
$comments_query = new WP_Comment_Query;
$comments = $comments_query->query( $args );
 
// Comment Loop
if ( have_comments() ){
 foreach ( $comments as $comment ) {
    
   
 echo '<div class="userkomentet">';

 echo get_avatar( $comment, 32 );

 //echo get_author_posts_url( get_the_author_meta( 'ID' ) );
  // echo get_avatar( get_the_author_meta( 'ID' ), 32 );
   //echo get_avatar($user->ID, 59); 
    

  
    echo '<div class="d-flex flex-column">';
    echo '<h5 class="d-flex"><b>' . wpse_use_user_real_name( $author, $comment_id, $comment ). '<p><small>' .get_comment_date(). "  ". get_comment_time() . '</small></p></b></h5>';
    
    echo '<p id="koment">' . $comment->comment_content . '</p>';
    echo '</div>';
    echo '</div>';

 
    

 echo '<hr>';
 }
} else {
 echo '';
}
  ?>
                </div>
</div>
                <br>
                <div id="komentet" class="komento">
                    <?php 
                                comment_form(array(
                                    
                                    add_filter( 'comment_form_logged_in', '__return_empty_string' ),
                                    '<div>',
                                    '<div class="imgcomment"><img src="'. get_avatar_url( $current_user->user_email, 40 ) . '"</img></div>',
                                    'title_reply' => '',
                                    'comment_field' => '<p class="d-flex">' .
                                    '<label for="comment">' . __( '' ) . '</label> <br>' .
                                    ' <input type="text" class="post-comment-new" id="inputComment" name="comment" placeholder=" Komenti juaj..."></input>' .
                                    '</p>',
                                    'comment_notes_after' => '',
                                    'label_submit'=>__('Komento'),
                                    '</div>',
                                
                            ));
                              ?>
                </div>
            </div>
        </div>
</div>
</article>



<?php 

if(isset($_GET['zgjedhKomentin'])){
 
if( have_comments() )
{       
$post_id = get_the_id();
$comments = get_comments('post_id=$post_id&status=approve');
if ($comments) { 
    $ndx = mt_rand(1,sizeof($comments)) - 1;
$comment = $comments[$ndx]; 
}


?>
<div class="userkomentet random" style="margin-left:28%;">
    <?php

echo get_avatar( $comment, 32 );

 echo '<div class="d-flex flex-column">';
    echo '<h5 class="d-flex"><b>' . wpse_use_user_real_name( $author, $comment_id, $comment ). '<p><small>' .get_comment_date(). "  ". get_comment_time() . '</small></p></b></h5>';
    
    echo '<p id="koment">' . $comment->comment_content . '</p>';
    echo '</div>';
    echo '</div>';
}
}
    ?>




</div>

<div class="right-side2 shadow" style="position: absolute; top:110px;">

    <h3 style="text-align:center;">Artikuj të ngjashëm</h3>
    <?php

//get the taxonomy terms of custom post type
$customTaxonomyTerms = wp_get_object_terms( $post->ID, 'kategorite', array('fields' => 'ids') );

//query arguments
$args = array(
    'post_type' => 'postimet',
    'post_status' => 'publish',
    'posts_per_page' => 3,
    'orderby' => 'rand',
    'tax_query' => array(
        array(
            'taxonomy' => 'kategorite',
            'field' => 'id',
            'terms' => $customTaxonomyTerms
        )
    ),
    'post__not_in' => array ($post->ID),
);

//the query
$relatedPosts = new WP_Query( $args );

//loop through query
if($relatedPosts->have_posts()){
    
    while($relatedPosts->have_posts()){ 
        $relatedPosts->the_post();

        get_template_part('meshumeposte', get_post_format());

    }
    
}else{
    //no posts found
}
 
//restore original post data
wp_reset_postdata();

?></div>
</div>
</div>



<?php  get_footer();   ?>