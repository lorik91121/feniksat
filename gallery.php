
<li>
    <a href="#modal<?php the_ID();?>" data-toggle="modal">
        <img src="<?php the_post_thumbnail_url();?>" loading="lazy">
    </a>
</li>
    



<?php $prev_post = get_adjacent_post( true, '', false, 'kategorite' ); ?>
<?php $next_post = get_adjacent_post( true, '', true, 'kategorite' ); ?>

<div id="modal<?php the_ID();?>" class="modal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-lg bg-transparent mobileL">
        <div class="modal-content modal-kategori">
            <div class="modal-header costumModalHeader">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" id="butoni-close" class="pull-right" data-dismiss="modal" aria-label="Close" >
                    <span aria-hidden="true" style="font-size:50px; color:white;">&times;</span>
                </button>
            </div>

            <div class="modal-body mobileL2" >
                <div class="d-flex flex-row justify-content-between align-items-center">                       
                    <a class="arrows" id="left" href="#modal<?php echo  $prev_post->ID; ?>" data-dismiss="modal" data-toggle="modal"><i class="bi-chevron-left"></i></a>
                    
                    <div class="modal-div">
                        <img class="modalImg img-fluid" src="<?php the_post_thumbnail_url();?>">
                            <div class="over-content">
                            <?php if (!is_user_logged_in()) { ?>
                                <a href="#myModal2" class="name" data-toggle="modal" data-dismiss="modal">
                                <img class="autori-modal" src="<?php $auth_id = $post->post_author; print get_avatar_url(get_the_author_meta('ID', $auth_id)); ?>" />
                                <?php $auth_id = $post->post_author; echo get_the_author_meta( 'display_name', $auth_id ); ?>    
                                </a>
                                <p><a href="#myModal2" data-toggle="modal" data-dismiss="modal"></i>Postimi i plote</a></p>
                            <?php }else{ ?>
                                <?php $auth_id = $post->post_author; ?>
                                <a href="<?=get_author_posts_url($auth_id);?>" class="name">
                                <img class="autori-modal" src="<?php $auth_id = $post->post_author; print get_avatar_url(get_the_author_meta('ID', $auth_id)); ?>" />
                                <?php $auth_id = $post->post_author; echo get_the_author_meta( 'display_name', $auth_id ); ?>    
                                </a>
                                <p><a href="<?php the_permalink();?>"><i class="bi bi-arrow-return-right"></i>Postimi i plote</a></p>
                            <?php } ?>
                            </div>
                    </div>
                    
                    <a class="arrows" id="right" href="#modal<?php echo $next_post->ID; ?>" data-dismiss="modal" data-toggle="modal"><i class="bi-chevron-right"></i></a> 
                </div>
            </div>
        </div>
    </div>
</div>