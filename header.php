<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<div class="m-4 ">
			<nav class="navbar navbar-default navbar-fixed-top" id="nav2">
				<div class="container" >
					<!--<button type="img" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" disabled>
						<a class="navbar-brand" href="#"><?php //the_custom_logo(); ?></a>
					</button>-->
				<div class="container-fluid navP">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="mobileU"><?php the_custom_logo(); ?></div>
				</div>
					<div class="nav1 collapse navbar-collapse navbar-expand-xl" id="bs-example-navbar-collapse-1">
						<!-- Brand and toggle get grouped for better mobile display -->
						<span class="logo1">
						<?php the_custom_logo(); ?>
						</span>
						<?php if(!is_user_logged_in()){?>
							<div class=" navbar-right d-inline">
								<button id="login-button" class="btn rounded-pill btn-md" href="#myModal2" data-toggle="modal">Kyqu</button>
								<button id="register-button" class="btn rounded-pill btn-md" href="#myModal1" data-toggle="modal">Regjistrohu</button>
							</div>
						<?php }?>
						<?php
						wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'container' => false,
								'menu_class' => 'nav navbar-nav navbar-right d-inline',
								'walker' => new Walker_Nav_Primary()
							)
						);
						?>
					</div>
				</div>
			</nav>
		<!-- Button trigger modal -->	
			<!-- Modal -->
						<div id="myModal1" class="modal fade" tabindex="-1">
							<div class="modal-dialog mobileL w-50 p-3 ">
								<div class="modal-content">
									<!-- The modal close x-->
										<div class="modal-header costumModalHeader MforMobile">
											<h5 class="modal-title" id="exampleModalLabel"></h5>
											<button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
									<div class="modal-body mobileL2">
										<!-- <div class="login-page"> -->
										<div class="form1">
											<form id="dhuro_registration_form" class="register-form" action="" method="POST">

												<h1 class="title">Register</h1>

												<input name="user_Login" id="user_Login" class="user_Login" placeholder="Perdoruesi..." type="text" />


												<input name="user_email" id="user_email" class="user_email" placeholder="perdoruesi@email.com" type="email" />


												<input name="user_first" id="user_first" type="text" placeholder="Emri..." class="user_first" />

												<input name="user_last" id="user_last" type="text" placeholder="Mbiemri..." class="user_last" />

												<input name="user_pass" id="password" class="password" placeholder="Fjalekalimi..." type="password" />

												<input name="user_pass_confirm" id="password_again" class="password_again" placeholder="Konfirmo Fjalekalimin..." type="password" />

												<input type="hidden" name="dhuro_csrf" value="<?php echo wp_create_nonce('dhuro-csrf'); ?>" />

												<button type="submit" name="btn_submit">Register</button>

												<p class="text-center my-3">Je i regjistruar?<a href="#myModal2" data-toggle="modal" data-dismiss="modal"> Kyqu </a></p>
												
											</form>
										</div>
										<!-- </div> -->
									</div>

								</div><!-- /.modal-dialog -->
							</div><!-- /.modal -->
						</div>

						<div id="myModal2" class="modal fade" tabindex="-1">

							<div class="modal-dialog mobileL  w-50 p-3">
								<div class="modal-content">
									<!-- The modal close x-->
									<div class="modal-header costumModalHeader MforMobile">
											<h5 class="modal-title" id="exampleModalLabel"></h5>
											<button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
									<div class="modal-body mobileL2">
										<!-- <div class="login-page"> -->

										
													<div class="form1">
														<form class="login-form" method="POST">

															<h1 class="title">Log In</h1>

															<input type="text" id="username" name="username" placeholder="Perdoruesi...">

															<input type="password" id="password" name="password" placeholder="Fjalekalimi...">

															<button type="submit" name="btn_submit">Login</button>

															<p class="text-center my-3">Nuk je i regjistruar?<a href="#myModal1" data-toggle="modal" data-dismiss="modal"> Regjistrohu </a></p>
														</form>
													</div>

										<!-- </div> -->
									</div>

								</div><!-- /.modal-dialog -->
							</div><!-- /.modal -->
						</div>
					</div>
		</div>
	</div>
	<div class="container-sm position-fixed info" id="shto" >
		<table  class="pull-right popup dropdown-submenu">
			  <tr >
				<th rowspan="3"><a href="<?php echo get_home_url() . '/profili';?>"><?php if (is_user_logged_in()) {$user = wp_get_current_user(); echo get_avatar($id_or_email=$user->ID, 60);}?></a></th>
				<td><?php if (is_user_logged_in()) {$user = wp_get_current_user(); echo $user->display_name;}?></td>
			  </tr>
			  <tr>
				<td><a href="<?php echo get_home_url() . '/profili';?>">Edit Profile</a></td>
			  </tr>
			  <tr>
				<?php if (is_user_logged_in()) {?>
				<td><a href="<?php echo wp_logout_url($redirect = 'index.php'); ?>">Log Out</a></td>
				<?php } ?>
			  </tr>
		</table>
	</div></br>
	<header id="masthead" class="site-header">
		<div class="site-branding">
			<?php

			if (is_front_page() && is_home()) :
			?>

			<?php
			else :
			?>
				
			<?php
			endif;
			$dhuro_description = get_bloginfo('description', 'display');
			if ($dhuro_description || is_customize_preview()) :
			?>

			<?php endif; ?>
		</div><!-- .site-branding -->
	</header><!-- #masthead -->