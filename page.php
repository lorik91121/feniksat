<?php

get_header();
?>

	<main id="primary" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
		<div>
		<?php
			$labels = array(
		'post_type' => 'Postimet',
        'post_status' => 'publish',
        'posts_per_page' => 8, 
        'orderby' => 'title', 
        'order' => 'ASC', 
	);
		$labels = new WP_Query( $labels ); 
        
    while ( $labels->have_posts() ) : $labels->the_post(); 
        print the_title(); 
        the_excerpt(); 
    endwhile;

    wp_reset_postdata(); ?></div>

	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
