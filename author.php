<?php
get_header();
// Set the Current Author Variable $curauth
$pg = $_GET['posts_per_page'];
$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
$args = array(  
'post_type' => 'postimet',
'post_status' => 'publish',
'author'  =>  $curauth->ID,
'posts_per_page' => $pg ,
);

$cs = new WP_Query( $args ); 
?>

	<div class="author-profile-card2">
		
		<div class="author-photo2">
		<?php echo get_avatar( $curauth->user_email , 150 ); ?>
		<h1><?php echo $curauth->display_name; ?></h1>
		</div>
		<div class="textU"><p class="author-p"><strong>Website:&nbsp;</strong> </br>*&nbsp;<a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a><br />
		<strong>Emri Mbiemri:&nbsp;</strong></br>*&nbsp;<i><?php echo $curauth->first_name ; ?>&nbsp;<?php echo $curauth->last_name; ?></i></p></div>
	</div>
<div class="site-main2">   
	<h2 class="postf">Postimet e fundit nga <?php echo $curauth->display_name; ?><small><a href="<?php if($pg <= 4){echo $_SERVER['REQUEST_URI']. '?posts_per_page=50';}?>"  >Të gjitha</a></small></h2>
	<div class="row row-cols-1 row-cols-md-3 cen">
         <?php if ( $cs -> have_posts() ) : while ( $cs->have_posts() ) : $cs->the_post(); ?>
		 
				<?php get_template_part('template-parts/content','author'); ?>
		
		<?php endwhile; 
 
		// Previous/next page navigation.
		//the_posts_pagination();
		else: ?>
		<p><?php _e('Nuk ka postime nga ky përdorues.'); ?></p>

	<?php endif; wp_reset_postdata();?>
	 </div>
</div>
<?php
get_footer();
