<?php 
/*

Template Name: Custom posts

*/
 get_header();  ?>
	<div class="row" style="padding: 20px;">


	<?php 
		
	$args = array('post_type' => 'postimet', 'post_per_page' => 3 );
	$loop = new WP_Query( $args );
	
	if( $loop->have_posts() ):
		
		while( $loop->have_posts() ): $loop->the_post(); ?>
			
			<?php get_template_part('content', 'taxonomies'); ?>
		
		<?php endwhile;
		
	endif;
		
	wp_reset_postdata();
		
	?>


</div>	
<?php  get_footer();  ?>