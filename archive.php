<?php


get_header();
?>
	<div class="site-main2">
		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header>

			<?php
		
			while ( have_posts() ) :
				the_post();
?>
			
				<?php get_template_part('template-parts/content','archive'); ?>

	<?php		endwhile;

			the_posts_pagination();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		wp_reset_postdata();
		?>
	</div>

<?php
get_footer();