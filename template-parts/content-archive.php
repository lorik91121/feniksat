<!-- Archive Page-->
<div class="card mb-3 postA" >
					  <div class="row g-0">
						<div class="col-md-4">
						  <a href="<?php the_permalink();?>"><?php if( has_post_thumbnail() ): ?>
						<img class="mr-3 img-fluid post-thumb d-none d-md-flex" src="<?php echo the_post_thumbnail_url('thumbnail'); ?>" alt="image">
					
						<?php endif; ?></a>
						</div>
						<div class="col-md-8">
						  <div class="card-body">
							<h3 class="card-title"><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3>
							<p class="card-text"><small class="text-muted"><?php the_date();?> &nbsp; <?php the_tags(); ?> &nbsp; <?php comments_number();?> </small></p>
							<p class="card-text"><?php the_excerpt(); ?></p>
							
							<a class="btn backbtn1" href="<?php the_permalink();?>">Më shumë →</a>
						  </div>
						</div>
					  </div>
				</div>

