<?php

	/* 

	Template Name:  Rreth Nesh Page

	*/
	get_header(); 

    $banner_image = get_field('banner_image');
    $banner_title = get_field('banner_title');
    $banner_description = get_field('banner_description');
    $first_title = get_field('first_title');
    $rreth_nesh_description = get_field('rreth_nesh_description');
    $second_title = get_field('second_title');
    $third_title = get_field('third_title');

	?>


<header>
    <?php if($banner_image):?>
    <div class="header-image" style="background-image: url(<?php echo $banner_image['url'];?>)">
        <?php endif; ?>
        <?php if($banner_title):?>
        <h1>
            <?php echo $banner_title;?>
        </h1>
        <?php endif; ?>
        <?php if($banner_description):?>
        <span>
            <?php echo $banner_description;?>
        </span>
        <?php endif; ?>
    </div>
</header>


<div class="rrethnesh-text">
    <?php if($first_title):?>
    <h1>
        <?php echo $first_title;?>
    </h1>
    <?php endif; ?>

    <?php if($rreth_nesh_description):?>
    <p>
        <?php echo $rreth_nesh_description;?>
    </p>
    <?php endif; ?>
</div>

<div class="title">
    <?php if($second_title):?>
    <h1>
        <?php echo $second_title;?>
    </h1>
    <?php endif; ?>
</div>

<div class="team-leaders">
    <?php if (have_rows('team_leaders')):?>
        <?php while(have_rows('team_leaders')): the_row();

            $name = get_sub_field('name');                
            $image = get_sub_field('image');
            $picture = $image['url'];
        ?>
            <div class="flip-card">
                <div class="flip-card-inner">
                    <div class="flip-card-front">
                        <?php if($image):?>
                            <img src="<?php echo $picture;?>">
                        <?php endif;?>
                    </div>
                    <div class="flip-card-back">
                        <?php if($name):?>
                            <h1>
                                <?php echo $name;?>
                            </h1>
                        <?php endif;?>
                    </div>
                </div>
            </div>

        <?php endwhile;?>

    <?php endif;?>

</div>

<div class="title">
    <?php if($third_title):?>
    <h1>
        <?php echo $third_title;?>
    </h1>
    <?php endif; ?>
</div>

<div class="team-members">

    <?php if (have_rows('team_members')):?>
        <?php while(have_rows('team_members')): the_row();

            $name = get_sub_field('name');                  
            $image = get_sub_field('image');
            $picture = $image['url'];
        ?>
            <div class="flip-card2">
                <div class="flip-card-inner">
                    <div class="flip-card-front">
                        <img src="<?php echo $picture;?>">
                    </div>
                    <div class="flip-card-back">
                        <?php if($name):?>
                            <h3>
                                <?php echo $name;?>
                            </h3>
                        <?php endif;?>
                    </div>
                </div>
            </div>

        <?php endwhile;?>

    <?php endif;?>

</div>

<?php get_footer(); ?>