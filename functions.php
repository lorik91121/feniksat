<?php
  
if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
} 

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function add_theme_scripts()
{
	

	wp_enqueue_style('bootsrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css');
	
	wp_enqueue_style('bootsrap', 'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css');
	wp_enqueue_script('bootstraps', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js');
	//----
	wp_enqueue_script('feniksat-profil', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js');

	wp_enqueue_script('feniksat-header', 'https://kit.fontawesome.com/a076d05399.js');
	
	wp_enqueue_script('bootstraps', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js');
	

	//---
	
	wp_enqueue_script('jquery');
	
	wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), '3.3.4', true);
	wp_enqueue_script('jqueryjs', get_template_directory_uri() . '/assets/js/jquery.js', array(), '1.0', true);
	//css
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '1.1', 'all');
	wp_enqueue_style('customstyle', get_template_directory_uri() . '/assets/css/awesome.css', array(), '1.1', 'all');

	wp_enqueue_style( 'feniksat-style', get_template_directory_uri() . '/assets/css/main.css', array(), '1.1', 'all');

    wp_enqueue_script( 'feniksat-customizer', get_template_directory_uri() . '/assets/js/customizer.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'feniksat-preview', get_template_directory_uri() . '/assets/js/preview.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'feniksat-validate', get_template_directory_uri() . '/assets/js/validate.js', array(), _S_VERSION, true );

}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

//Check if user is logged-in to show different menu 
function my_wp_nav_menu_args( $args = '' ) {
 
if( is_user_logged_in() ) { 
    $args['menu'] = 'logedin';
} else { 
    $args['menu'] = 'Menu';
} 
    return $args;
}
add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' );

add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
  //if (!current_user_can('administrator') && !is_admin()) {
    show_admin_bar(false);
  
}


//Produktet qe mora
function my_custom_menu_item($items)
{
    if (is_user_logged_in()) {
        $user = wp_get_current_user();
			$post__in = wp_list_pluck( get_comments( array( 'user_id' => get_current_user_id() ) ),'comment_post_ID' );
			$args = array (
				'user_id'        => $user->ID,
				'post_status'    => 'approve',
				'number'         => '10',
			);

			// The Comment Query
			$comments = new WP_Comment_Query;
			$comments = $comments->query( $args );
			if ( ! empty( $post__in ) ) {
				$args = array(
					'post_type' => 'postimet',
					'posts_per_page' => 5,
					'post__in' => array_unique( 
						wp_list_pluck(
							get_comments( array(
									'user_id' => get_current_user_id() 
								)
							),       
							'comment_post_ID'
						)
					),
				);
					$query = new WP_Query( $args );
					$count = $query->post_count;
				};
        $name = $user->display_name; // or user_login , user_firstname, user_lastname
		$avatar = get_avatar($id_or_email=$user->ID, 25);
		$logoutU = wp_logout_url($redirect = 'index.php');
		$editU = get_home_url() . '/profili';
        $items .= '<li id="b" class="b"><a href="#" onclick="toggle_visibility(\'shto\');" >'.$avatar.' Welcome '.$name.' <span style="font-size:12px;">&#9660;</span></a></li>
					<li id="b2" class="mobileU"><a href="' . $editU . '">Edit Profile</a></li>
					<li id="b21" class="mobileU"><a href="' . $logoutU . '" >Log Out</a></li>
					<li><div class="dropdown show mydropdownjQuery">
						  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="bi bi-bag-check" style="font-size: x-large;"></i>
							<span class="csPostion badge rounded-pill badge-notification bg-danger">'. $count .'</span>
						  </a>
						  <div class="dropdown-menu animate slideIn maxWi" aria-labelledby="dropdownMenuLink">
						  <h4 class="titleh4">Produktet qe morra </h4>
						  <hr>
							'; 
				if ( $query != '' && $query->have_posts() ) {
							$i =0;
								 while ( $query->have_posts()) {
									$query->the_post();
									$link = get_the_permalink();
									$name = get_the_title();
									$imgP = get_the_post_thumbnail_url();	
									$comments = get_comments( array( 'user_id' => get_current_user_id(),'post_id' => get_the_ID() ) );
										
										foreach($comments as $c):
											$c22 =  $c->comment_date;
											$timestamp = strtotime($c22);
											$c2 = date("g:ia  j M Y",$timestamp);
											$i++;
										endforeach;
									$items .= '<div class="ofsetMar"><a class="dropdown-item" href="'. $link .'"><img class = "borderComent" src="'. $imgP .'"/>'. $name . '<small class="smallDrop"> '. $c2 .' </small></a></div>';
									
								};
							};
							wp_reset_postdata();
							
						$items .= ' </div>
						</div></li>';
    }

    return $items;
}

add_filter('wp_nav_menu_items', 'my_custom_menu_item');

//My activity
//Chane menu if user is loged-id, add this <li>
function my_custom_menu_item_activity($items)
{
    if (is_user_logged_in()) {
        $user = wp_get_current_user();
			$post__in = wp_list_pluck( get_comments( array( 'user_id' => get_current_user_id() ) ),'comment_post_ID' );
			$args = array (
				'user_id'        => $user->ID,
				'post_status'    => 'approve',
				'number'         => '10',
			);

			// The Comment Query
			$comments = new WP_Comment_Query;
			$comments = $comments->query( $args );
			if ( ! empty( $post__in ) ) {
				$args = array(
					'post_type' => 'postimet',
					'posts_per_page' => 5,
					'post__in' => array_unique( 
						wp_list_pluck(
							get_comments( array(
									'user_id' => get_current_user_id() 
								)
							),       
							'comment_post_ID'
						)
					),
				);
					$query = new WP_Query( $args );
					$count = $query->post_count;
				};
  
        $items .= '
					<li><div class="dropdown show mydropdownjQuery" style="margin-top:2px;">
						  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="bi bi-chat-square-heart"  style="font-size: x-large;"></i>
							<span class="csPostion badge rounded-pill badge-notification bg-danger">'. $count .'</span>
						  </a>
						  <div class="dropdown-menu animate slideIn maxWi" aria-labelledby="dropdownMenuLink">
						  <h4 class="titleh4">Aktivitetet e mia</h4>
						  <hr>
							'; 
				if ( $query != '' && $query->have_posts() ) {
							$i =0;
								 while ( $query->have_posts()) {
									$query->the_post();
									$link = get_the_permalink();
									$name = get_the_title();
									$imgP = get_the_post_thumbnail_url();	
									$comments = get_comments( array( 'user_id' => get_current_user_id(),'post_id' => get_the_ID() ) );
										
										foreach($comments as $c):
											$c22 =  $c->comment_date;
											$timestamp = strtotime($c22);
											$c2 = date("g:ia  j M Y",$timestamp);
											$i++;
										endforeach;
									$items .= '<div class="ofsetMar"><a class="dropdown-item" href="'. $link .'">
									<img class = "borderComent" src="'. $imgP .'"/>'. $name .
									 '<small class="smallDrop"> '. $c2 .' </small></a></div>';
									
								};
							};
							wp_reset_postdata();
							
						$items .= ' </div>
						</div></li>';
    }

    return $items;
}

add_filter('wp_nav_menu_items', 'my_custom_menu_item_activity');

//Notifications
function my_custom_menu_item_produkti($items)
{
    if (is_user_logged_in()) {
        
		$user = wp_get_current_user();
	

			$args = array(
				'post_author' => get_current_user_id(),
				
				
			);
			$comments = get_comments( $args );
			
			global $wpdb, $post, $current_user;
get_currentuserinfo();
$userId = $current_user->ID;

$where = 'WHERE comment_approved = 1 AND user_id = ' . $userId ;
$comment_count = $wpdb->get_var("SELECT COUNT( * ) AS total 
                                 FROM {$wpdb->comments}
                                 {$where}");
			
			  $query = new WP_Query( $comments );
			// $count = $query->comment_count;
			// $comments_count = wp_count_comments();
			


			
 
        $items .= '	<li><div class="dropdown show mydropdownjQuery">
						  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					
							<i class="bi bi-bell"  style="font-size: x-large;"></i>
							<span class="csPostion badge rounded-pill badge-notification bg-danger">'.  $comment_count  .'</span>
						  </a>
						  <div class="dropdown-menu animate slideIn maxWi" aria-labelledby="dropdownMenuLink">
						  <h4 class="titleh4">Njoftimet</h4>
						  <hr>
							'; 	
											//$test = 	wp_list_comments( $list_args, $comments );
									
								
									foreach($comments as $comm){
						
									$autori =  get_avatar_url($comm->user_id);
										$items .= '<div class="ofsetMar"><a class="dropdown-item" href="'. $link .'">
										<img class = "borderComent" src="'.  $autori  .'"/>'. $comm->comment_author .'<small class="smallDrop" style="font-size:10px;"> '.$comm->comment_date .'</small></a></div>';
										
									}
									};
								

								
							$items .= ' </div>
							</div></li>';
		
	
		return $items;
}
add_filter('wp_nav_menu_items', 'my_custom_menu_item_produkti');



function dhuro_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on dhuro, use a find and replace
		* to change 'dhuro' to the name of your theme in all the template files.
		*/

	add_theme_support('menus');
	
	register_nav_menu('primary', 'Primary Header Navigation');
	register_nav_menu('secondary', 'Footer Navigation');

	load_theme_textdomain('dhuro', get_template_directory() . '/languages');

	// Add default posts and comments RSS feed links to head.
	add_theme_support('automatic-feed-links');

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support('title-tag');

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support('post-thumbnails');

	// This theme uses wp_nav_menu() in one location.
	

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'dhuro_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support('customize-selective-refresh-widgets');

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 50,
			'width'       => 50,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action('after_setup_theme', 'dhuro_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function dhuro_content_width()
{
	$GLOBALS['content_width'] = apply_filters('dhuro_content_width', 640);
}
add_action('after_setup_theme', 'dhuro_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function dhuro_widgets_init()
{
	register_sidebar(
		array(
			'name'          => esc_html__('Sidebar', 'dhuro'),
			'id'            => 'sidebar-1',
			'description'   => esc_html__('Add widgets here.', 'dhuro'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action('widgets_init', 'dhuro_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function dhuro_scripts()
{
	wp_enqueue_style('dhuro-style', get_stylesheet_uri(), array(), _S_VERSION);
	wp_style_add_data('dhuro-style', 'rtl', 'replace');

/* 	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
 */
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
	
}
add_action('wp_enqueue_scripts', 'dhuro_scripts');

// /**
//  * Implement the Custom Header feature.
//  */
// require get_template_directory() . '/inc/custom-header.php';

// /**
//  * Custom template tags for this theme.
//  */
// require get_template_directory() . '/inc/template-tags.php';

// /**
//  * Functions which enhance the theme by hooking into WordPress.
//  */
// require get_template_directory() . '/inc/template-functions.php';

// /**
//  * Customizer additions.
//  */
// require get_template_directory() . '/inc/customizer.php';

// /**
//  * Load Jetpack compatibility file.
//  */
// if ( defined( 'JETPACK__VERSION' ) ) {
// 	require get_template_directory() . '/inc/jetpack.php';
// }
// function add_login_check()
// {
//     if ( is_user_logged_in() && is_front_page() ) {
//         wp_redirect(home_url().'/ballina');
//         exit;
//     }
// }

// add_action('wp', 'add_login_check');

// user registration login form
function dhuro_registration_form()
{

	// only show the registration form to non-logged-in members
	if (!is_user_logged_in()) {

		// check if registration is enabled
		$registration_enabled = get_option('users_can_register');

		// if enabled
		if ($registration_enabled) {
			$output = dhuro_registration_fields();
		} else {
			$output = __('User registration is not enabled');
		}
		return $output;
	}
}
add_shortcode('register_form', 'dhuro_registration_form');

// registration form fields
function dhuro_registration_fields()
{

	ob_start(); ?>	
	<?php
	return ob_get_clean();
}

// register a new user
function dhuro_add_new_user()
{
	if (isset($_POST["user_Login"]) && wp_verify_nonce($_POST['dhuro_csrf'], 'dhuro-csrf')) {
		$user_login		= $_POST["user_Login"];
		$user_email		= $_POST["user_email"];
		$user_first 	    = $_POST["user_first"];
		$user_last	 	= $_POST["user_last"];
		$user_pass		= $_POST["user_pass"];
		$pass_confirm 	= $_POST["user_pass_confirm"];

		// this is required for username checks
		require_once(ABSPATH . WPINC . '/registration.php');

		if (username_exists($user_login)) {
			// Username already registered
			echo "<script>alert(' Username already registered. Please try again!');</script>";
		}
		if (!validate_username($user_login)) {
			// invalid username
			echo "<script>alert(' Invalid username. Please try again!');</script>";
		}
		if ($user_login == '') {
			// empty username
			echo "<script>alert(' Empty Username. Please try again!');</script>";
		}
		if (!is_email($user_email)) {
			//invalid email
			echo "<script>alert(' Invalid email. Please try again!');</script>";
		}
		if (email_exists($user_email)) {
			//Email address already registered
			echo "<script>alert(' Email address already registred. Please try again!');</script>";
		}
		if ($user_pass == '') {
			// passwords do not match
			echo "<script>alert(' Please enter a password!');</script>";
		}
		if ($user_pass != $pass_confirm) {
			// passwords do not match
			echo "<script>alert(' Passwords do not match. Please try again!');</script>";
		}

		$errors = dhuro_errors()->get_error_messages();

		// if no errors then cretate user
		if (empty($errors)) {

			$new_user_id = wp_insert_user(
				array(
					'user_login'		=> $user_login,
					'user_pass'	 		=> $user_pass,
					'user_email'		=> $user_email,
					'first_name'		=> $user_first,
					'last_name'			=> $user_last,
					'user_registered'	=> date('Y-m-d H:i:s'),
					'role'				=> 'author'
				)
			);
			if ($new_user_id) {
				// send an email to the admin
				wp_new_user_notification($new_user_id);

				// log the new user in
				wp_setcookie($user_login, $user_pass, true);
				wp_set_current_user($new_user_id, $user_login);
				do_action('wp_login', $user_login);

				// send the newly created user to the home page after logging them in
				wp_redirect(home_url());
				exit;
			}
		}
	}
}
add_action('init', 'dhuro_add_new_user');

// used for tracking error messages
function dhuro_errors()
{
	static $wp_error; // global variable handle
	return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
}

// displays error messages from form submissions
function dhuro_register_messages()
{
	if ($codes = dhuro_errors()->get_error_codes()) {
		echo '<div class="dhuro_errors">';
		// Loop error codes and display errors
		foreach ($codes as $code) {
			$message = dhuro_errors()->get_error_message($code);
			echo '<span class="error"><strong>' . __('Error') . '</strong>: ' . $message . '</span><br/>';
		}
		echo '</div>';
	}	
}

function dhuro_login_user()
{
	global $user_ID;
global $wpdb;

if(!$user_ID){

    if(isset($_POST["btn_submit"])){
         
    $username = $wpdb->escape($_POST['username']);
    $password = $wpdb->escape($_POST['password']);
    
    $login_array = array();
    $login_array['user_login'] = $username;
    $login_array['user_password'] = $password;

    $verify_user = wp_signon($login_array, true);

    if(!is_wp_error($verify_user)){

        echo "<script>window.location = '".site_url()."/ballina'</script>";
    }else{

        echo "<p>Invalid Username/Password</p>";

    }

    }
    else{

	}
}
}
add_action('init', 'dhuro_login_user');

/*
	==========================================
	 Include Walker file
	==========================================
*/
require get_template_directory() . '/template-parts/walker.php';

/*
	==========================================
	 Add Costum avatar
	==========================================
*/
// 1. Enqueue the needed scripts.
add_action( "admin_enqueue_scripts", "ayecode_enqueue" );
function ayecode_enqueue( $hook ){
	// Load scripts only on the profile page.
	if( $hook === 'profile.php' || $hook === 'user-edit.php' ){
		add_thickbox();
		wp_enqueue_script( 'media-upload' );
		wp_enqueue_media();
	}
}

// 2. Scripts for Media Uploader.
function ayecode_admin_media_scripts() {
	?>
	<script>
		jQuery(document).ready(function ($) {
			$(document).on('click', '.avatar-image-upload', function (e) {
				e.preventDefault();
				var $button = $(this);
				var file_frame = wp.media.frames.file_frame = wp.media({
					title: 'Select or Upload an Custom Avatar',
					library: {
						type: 'image' // mime type
					},
					button: {
						text: 'Select Avatar'
					},
					multiple: false
				});
				file_frame.on('select', function() {
					var attachment = file_frame.state().get('selection').first().toJSON();
					$button.siblings('#ayecode-custom-avatar').val( attachment.sizes.thumbnail.url );
					$button.siblings('.custom-avatar-preview').attr( 'src', attachment.sizes.thumbnail.url );
				});
				file_frame.open();
			});
		});
	</script>
	<?php
}
add_action( 'admin_print_footer_scripts-profile.php', 'ayecode_admin_media_scripts' );
add_action( 'admin_print_footer_scripts-user-edit.php', 'ayecode_admin_media_scripts' );


// 3. Adding the Custom Image section for avatar.
function custom_user_profile_fields( $profileuser ) {
	?>
	<h3><?php _e('Custom Local Avatar', 'ayecode'); ?></h3>
	<table class="form-table ayecode-avatar-upload-options">
		<tr>
			<th>
				<label for="image"><?php _e('Custom Local Avatar', 'ayecode'); ?></label>
			</th>
			<td>
				<?php
				// Check whether we saved the custom avatar, else return the default avatar.
				$custom_avatar = get_the_author_meta( 'ayecode-custom-avatar', $profileuser->ID );
				if ( $custom_avatar == '' ){
					$custom_avatar = get_avatar_url( $profileuser->ID );
				}else{
					$custom_avatar = esc_url_raw( $custom_avatar );
				}
				?>
				<img style="width: 96px; height: 96px; display: block; margin-bottom: 15px;" class="custom-avatar-preview" src="<?php echo $custom_avatar; ?>">
				<input type="text" name="ayecode-custom-avatar" id="ayecode-custom-avatar" value="<?php echo esc_attr( esc_url_raw( get_the_author_meta( 'ayecode-custom-avatar', $profileuser->ID ) ) ); ?>" class="regular-text" />
				<input type='button' class="avatar-image-upload button-primary" value="<?php esc_attr_e("Upload Image","ayecode");?>" id="uploadimage"/><br />
				<span class="description">
					<?php _e('Please upload a custom avatar for your profile, to remove the avatar simple delete the URL and click update.', 'ayecode'); ?>
				</span>
			</td>
		</tr>
	</table>
	<?php
}
add_action( 'show_user_profile', 'custom_user_profile_fields', 10, 1 );
add_action( 'edit_user_profile', 'custom_user_profile_fields', 10, 1 );


// 4. Saving the values.
add_action( 'personal_options_update', 'ayecode_save_local_avatar_fields' );
add_action( 'edit_user_profile_update', 'ayecode_save_local_avatar_fields' );
function ayecode_save_local_avatar_fields( $user_id ) {
	if ( current_user_can( 'edit_user', $user_id ) ) {
		if( isset($_POST[ 'ayecode-custom-avatar' ]) ){
			$avatar = esc_url_raw( $_POST[ 'ayecode-custom-avatar' ] );
			update_user_meta( $user_id, 'ayecode-custom-avatar', $avatar );
		}
	}
}

// 5. Set the uploaded image as default gravatar.
add_filter( 'get_avatar_url', 'ayecode_get_avatar_url', 10, 3 );
function ayecode_get_avatar_url( $url, $id_or_email, $args ) {
	$id = '';
	if ( is_numeric( $id_or_email ) ) {
		$id = (int) $id_or_email;
	} elseif ( is_object( $id_or_email ) ) {
		if ( ! empty( $id_or_email->user_id ) ) {
			$id = (int) $id_or_email->user_id;
		}
	} else {
		$user = get_user_by( 'email', $id_or_email );
		$id = !empty( $user ) ?  $user->data->ID : '';
	}
	//Preparing for the launch.
	$custom_url = $id ?  get_user_meta( $id, 'ayecode-custom-avatar', true ) : '';
	
	// If there is no custom avatar set, return the normal one.
	if( $custom_url == '' || !empty($args['force_default'])) {
		return esc_url_raw( get_template_directory_uri() . '/assets/images/defaultavatar.png' ); 
	}else{
		return esc_url_raw($custom_url);
	}
}
/*
	====================================================================================
*/

/*
	==========================================
	 POST TYPE
	==========================================
*/
function dhuro_custom_post_type (){
	
	$labels = array(
		'name' => 'Postimet',
		'singular_name' => 'Postimet',
		'add_new' => 'Add Item',
		'all_items' => 'All Items',
		'add_new_item' => 'Add Item',
		'edit_item' => 'Edit Item',
		'new_item' => 'New Item',
		'view_item' => 'View Item',
		'search_item' => 'Search Postimet',
		'not_found' => 'No items found',
		'not_found_in_trash' => 'No items found in trash',
		'parent_item_colon' => 'Parent Item'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => true,
		'publicly_queryable' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'supports' => array(
			'title',
			'editor',
			'excerpt',
			'thumbnail',
			'comments',
			'revisions',
		),
		//'taxonomies' => array('category', 'post_tag'),
		'menu_position' => 5,
		'exclude_from_search' => false
	);
	register_post_type('postimet',$args);
}
add_action('init','dhuro_custom_post_type');


/*
	==========================================
	 TAXONOMY
	==========================================
*/

function postimet_custom_taxonomies(){

	//add new taxonomy hierarchical
	$labels = array(
		'name' => 'kategorite',
		'search_items' => 'Search ',
		'all_items'=>'All Categories',
		'parent_item'=>'Parent Type',
		'parent_item_colon'=>'Parent Type:',
		'edit_item'=>'Edit Categories',
		'update_item'=>'Update Categories',
		'add_new_item'=>'Add New Categories',
		'new_item_name'=>'New Categories',
		'menu_item'=>'Categories'
	);

	$args = array(
		'hierarchical'=> true,
		'labels'=>$labels,
		'show_ui'=> true,
		'show_admin_column'=>true,
		'query_var'=> true,
		'rewrite'=> array('slug'=>'kategorite')
	);


	register_taxonomy('kategorite', array('postimet'), $args);

	//add new taxonomy that is not hierarchical

	register_taxonomy('tagu',array('postimet'), array(
		'label'=>'Tagu',
		'rewrite'=> array('slug'=>'tagu'),
		'hierarchical'=> false
	));


}

add_action('init', 'postimet_custom_taxonomies');

/*
	==========================================
	Custom Term Function
	==========================================
*/

function dhuro_get_terms( $postID, $term ){
	
	$terms_list = wp_get_post_terms($postID, $term); 
	$output = '';
					
	$i = 0;
	foreach( $terms_list as $term ){ $i++;
		if( $i > 1 ){ $output .= ', '; }
		$output .=  $term->name ;
	}
	
	return $output;
	
}

/*
	==========================================
	Categories
	==========================================
*/



function dhuro_insert_category() {

  wp_insert_term( 'Femra','kategorite',
 array(

      'description' => '',

      'slug'    => 'femra'
    )
  );
  wp_insert_term( 'Meshkuj','kategorite',
  array(
 
	   'description' => '',
 
	   'slug'    => 'meshkuj'
	 )
   );
   wp_insert_term( 'Femije','kategorite',
   array(
  
		'description' => '',
  
		'slug'    => 'femije'
	  )
	);
 



}

add_action( 'after_setup_theme', 'dhuro_insert_category' );

add_filter( 'get_comment_author', 'wpse_use_user_real_name', 10, 3 ) ;


/*
	==========================================
	Display commentor's first name and last name
	==========================================
*/

function wpse_use_user_real_name( $author, $comment_id, $comment ) {

    $firstname = '' ;
    $lastname = '' ;


    //returns 0 for unregistered commenters
    $user_id = $comment->user_id ;

    if ( $user_id ) {

        $user_object = get_userdata( $user_id ) ;

        $firstname = $user_object->user_firstname ;

        $lastname = $user_object->user_lastname ; 

    }

    if ( $firstname || $lastname ) {

        $author = $firstname . ' ' . $lastname ; 

        //remove blank space if one of two names is missing
        $author = trim( $author ) ;

    }

    return $author ;

}
add_action( 'comment_form_logged_in_after', 'psot_comment_form_avatar' );
add_action( 'comment_form_after_fields', 'psot_comment_form_avatar' );
function psot_comment_form_avatar()
{
  ?>
   <div class="comment-avatar">
     <?php 
     $current_user = wp_get_current_user();
     if ( ($current_user instanceof WP_User) ) {
        echo get_avatar( $current_user->user_email, 32 );
     }
     ?>
   </div>
<?php
}


?>
